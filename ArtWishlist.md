# Art For TippingPigs
Some of this might be done with free (or low-priced) assets from the internet, but ideally, everything art-related is made in one matching style. There is also some leeway in what is strictly necessary and what is just a bonus. Additionally, when thinking about time that it takes to do these assets, some time for revisions needs to be accounted for as well. A suitable image format for Godot might be png. Other formats I am not sure about, but if you prefer something else, we can certainly try it!

In the list, I’ll sometimes suggest some motives/ideas, but of course I am most happy about any other creative ideas you might have! 

## Necessary Art:
	
Style suggestion: 
Semi-realistic but at least moderately cute, like the current pig-art placeholder. I’d suggest pixel art, for simplicity’s sake, dut doesn’t have to be if you are more comfortable/experienced with other! In any case though: 2D.

Menu Background:

	o Either purely static image, or image with animated elements
	o Suggestions: 
		o Take the current placeholder photos as inspiration: One or more lazy pigs lying on the shore in the shadow of a coconut palm. Waves. Towering blueish-white clouds on the horizon. Maybe a villager sitting nearby, herding the pig.
	o Suggested features: Pigs, Villagers, Island environment, …
	o Comments:
		o This is a more involved piece of art, especially with animated elements. Resolution is much higher than the other 
Island Background:

	o This should be a parallax background (i.e., a scrolling background with multiple layers of different speed) with 2-3 static images that will be animated later. Should look like closer clouds (moving faster) and further away clouds (moving slower). See here for an example: https://youtu.be/TWAvAXV2MLo?t=1595 
	o Suggestions:
		o 2-3 frames of different densities/distances of clouds. 
		o Bottom is the horizon + ocean; should fit with the ocean tiles, see below
Forest tiles

	o Tropical / pacific island forest.
	o Comments:
		o Middle bit + 8 surrounding sides and corners = 9 tiles; 
		o  Ideally, this comes with variation for different states representing forest health/tree cover, so 9^x; but this is definitely just a bonus.
Grass tiles:

	o Grass- or shrubland style tiles.
	o Comments:
		o Minimum requirement is 1 grass tile.
		o Ideally variations; possibly just for aesthetics, or to represent different states of tiles = 1 base tile + 1-3 variations
Coast:

	o The shoreline, sandy or rocky beach
	o Comments:
		o we need 8 tiles: all sides and corners
		o Ideally, we have a little wavy animation: this would include 3-4 frames of animation, x 3 for waves coming from the south, from the west and from the east. 
Ocean:

	o Minimum requirement is 1 tile
	o Ideally: animation of waves, ca. 3-4 frames are enough; also, more shallow/lighter water near the island might be nice looking, too: again just 1 tile necessary
Villager animations:

	o Reference/Style is tricky: most important is to avoid stereotypical depictions, I’d think. 
	o Moving Cycle (3-4 frames); Idling (2-3 frames)
	o Ideally: cutting animation, animation for leaving the island (e.g., setting sail on a traditional catamaran)
Pig animations:

	o The current placeholders are quite cute, you could use that as starting point or reference.
	o Moving Cycle (3-4 frames); Idling (2-3 frames)
	o Ideally: eating animation, animation for sailing away (i.e., a pig in a tiny boat sailing away), feast animation (i.e., villagers roasting pigs, or something less sad like villagers sitting around a feast)
UI Elements:

	o This is maybe a bonus altogether, because simple, toned-down elements are also possible in Godot
	o Ideally: frames and backgrounds for all info-boxes; art for all buttons; UI panel art for values-info at the top; background/syling for the plot (although not sure about implementation here)

